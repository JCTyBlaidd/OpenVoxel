package net.openvoxel.client.gui.widgets.group;

import net.openvoxel.client.gui.framework.GUIObject;
import net.openvoxel.client.gui.framework.ResizedGuiRenderer;
import net.openvoxel.client.gui.widgets.GUIObjectSizable;
import net.openvoxel.client.renderer.common.IGuiRenderer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by James on 08/04/2017.
 *
 * Area with a scroll bar
 */
public class GUIVScrollArea extends GUIObjectSizable {

	private List<GUIObject> subObjects;
	private float absOffset = 0;
	private float maxOffset = 0;
	private boolean isDirty = false;

	public GUIVScrollArea() {
		subObjects = new ArrayList<>();
	}

	public void add(GUIObject object) {
		subObjects.add(object);
	}

	@Override
	public void Draw(IGuiRenderer drawHandle) {
		DrawSquare(drawHandle,null,0xFF5500CC);
		final float screenWidth = drawHandle.getScreenWidth();
		final float screenHeight = drawHandle.getScreenHeight();
		final float X1 = getPosX(screenWidth);
		final float Y1 = getPosY(screenHeight);
		final float X2 = X1 + getWidth(screenWidth);
		final float Y2 = Y1 + getHeight(screenHeight);
		final int col = 0xFF446643;
		drawHandle.Begin(null);
		drawHandle.VertexRect(X1,X2,Y1,Y2,col);
		ResizedGuiRenderer resizedTess = new ResizedGuiRenderer(drawHandle);
		resizedTess.set(X1,Y1,X2-X1,Y2-Y1);
		drawHandle.pushScissor(
				(int)(X1*screenWidth),
				(int)(Y1*screenHeight),
				(int)((X2-X1)*screenWidth),
				(int)((Y2-Y1)*screenHeight)
		);
		for(GUIObject sub_object : subObjects) {
			sub_object.Draw(resizedTess);
		}
		drawHandle.popScissor();
	}

	@Override
	public void OnMouseMove(float newX, float newY, float oldX, float oldY,float screenWidth,float screenHeight) {
		final float X = getPosX(screenWidth);
		final float Y = getPosY(screenHeight);
		final float W = getWidth(screenWidth);
		final float H = getHeight(screenHeight);
		final float internal_xNew = (newX - X) / W;
		final float internal_yNew = (newY - Y - absOffset) / H;
		final float internal_xOld = (oldX - X) / W;
		final float internal_yOld = (oldY - Y - absOffset) / H;
		for(GUIObject object : subObjects) {
			object.OnMouseMove(internal_xNew,internal_yNew,internal_xOld,internal_yOld,screenWidth*W,screenHeight*H);
		}
	}

	@Override
	public void OnMousePress(float x, float y, float screenWidth, float screenHeight) {
		final float X = getPosX(screenWidth);
		final float Y = getPosY(screenHeight);
		final float W = getWidth(screenWidth);
		final float H = getHeight(screenHeight);
		final float internal_x = (x - X) / W;
		final float internal_y = (y - Y - absOffset) / H;
		for(GUIObject object : subObjects) {
			object.OnMousePress(internal_x,internal_y,screenWidth*W,screenHeight*H);
		}
	}

	@Override
	public void OnMouseRelease(float x, float y, float screenWidth, float screenHeight) {
		final float X = getPosX(screenWidth);
		final float Y = getPosY(screenHeight);
		final float W = getWidth(screenWidth);
		final float H = getHeight(screenHeight);
		final float internal_x = (x - X) / W;
		final float internal_y = (y - Y - absOffset) / H;
		for(GUIObject object : subObjects) {
			object.OnMouseRelease(internal_x,internal_y,screenWidth*W,screenHeight*H);
		}
	}

	@Override
	public boolean isDrawDirty() {
		boolean is_dirty = isDirty;
		isDirty = false;
		for(GUIObject object : subObjects) {
			is_dirty |= object.isDrawDirty();
		}
		return is_dirty;
	}
}
