package net.openvoxel.client.gui.menu;

import net.openvoxel.client.gui.framework.Screen;
import net.openvoxel.client.gui.widgets.display.GUIObjectImage;
import net.openvoxel.client.gui.widgets.input.GUIButton;

/**
 * Created by James on 14/09/2016.
 *
 * Load Scan For Multi-player Data
 */
public class ScreenMultiPlayer extends Screen{

	public GUIObjectImage background;

	public GUIButton connect;
	public GUIButton addServer;
	public GUIButton directConnect;

	public ScreenMultiPlayer() {

	}
/**
	public static class GUIMultiPlayerServer extends GUIObjectSizable {
		@Override
		public void Draw(GUIRenderer.GUITessellator drawHandle) {

		}
	}
**/
}
