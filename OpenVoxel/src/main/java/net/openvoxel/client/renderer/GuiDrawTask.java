package net.openvoxel.client.renderer;

import net.openvoxel.OpenVoxel;
import net.openvoxel.client.ClientInput;
import net.openvoxel.client.gui.framework.GUI;
import net.openvoxel.client.gui.framework.Screen;
import net.openvoxel.client.gui.util.ScreenDebugInfo;
import net.openvoxel.client.renderer.base.BaseGuiRenderer;
import net.openvoxel.client.renderer.common.GraphicsAPI;
import net.openvoxel.utility.CrashReport;
import net.openvoxel.utility.async.AsyncBarrier;

import java.util.Iterator;

public class GuiDrawTask implements Runnable {

	private final BaseGuiRenderer guiRenderer;
	private AsyncBarrier barrier;
	private int width;
	private int height;

	GuiDrawTask(GraphicsAPI api) {
		guiRenderer = api.getGuiRenderer();
	}

	public void update(AsyncBarrier barrier) {
		this.barrier = barrier;
		width = ClientInput.currentWindowFrameSize.x;
		height = ClientInput.currentWindowFrameSize.y;
	}

	@Override
	public void run() {
		try {
			//Check if dirty is possible
			boolean guiDirty = ScreenDebugInfo.debugLevel != ScreenDebugInfo.GUIDebugLevel.NONE;
			guiDirty |= !guiRenderer.allowDrawCaching();

			//Check if dirty
			if(!guiDirty) {
				for (Screen screen : GUI.getStack()) {
					guiDirty |= screen.isDrawDirty();
				}
			}

			//Update if dirty
			if(guiDirty) {
				guiRenderer.StartDraw(width,height);
				Screen limit = null;

				//Check if the screen hides
				for(Screen screen : GUI.getStack()) {
					if(screen.hidesPreviousScreens()) {
						limit = screen;
						break;
					}
				}

				//Draw in reverse order
				Iterator<Screen> iterate = GUI.getStack().descendingIterator();

				while(iterate.hasNext()) {
					if(limit == null) {
						Screen screen = iterate.next();
						screen.DrawScreen(guiRenderer);
					}else{
						Screen screen = iterate.next();
						if(screen == limit) {
							limit = null;
							screen.DrawScreen(guiRenderer);
						}
					}
				}

				//Debug Screen Renderer//
				ScreenDebugInfo.instance.DrawScreen(guiRenderer);
			}

			//Finish Draw
			guiRenderer.finishDraw(guiDirty);
		}catch(Exception ex) {
			CrashReport report = new CrashReport("Error Drawing GUI");
			report.caughtException(ex);
			OpenVoxel.reportCrash(report);
		}finally {
			barrier.completeTask();
		}
	}

}
