package net.openvoxel.client.textureatlas;

@Deprecated
public class BaseIcon implements Icon {

	public float U0 = 0;
	public float V0 = 0;
	public float U1 = 1;
	public float V1 = 1;

	public int animationCount = 1;
}
