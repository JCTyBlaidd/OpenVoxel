/**[VK::Vertex]**/

layout(location = 0) in vec2 position;
layout(location = 1) in vec2 uv_coord;
layout(location = 2) in vec4 col_input;

out gl_PerVertex {
    vec4 gl_Position;
};

layout(location = 0) out vec2 frag_UV;
layout(location = 1) out vec4 frag_Col;

void main() {
    frag_UV = uv_coord;
    frag_Col = col_input;
    gl_Position = vec4(position,0.0,1.0);
}

/**[VK::Fragment]**/

layout(location = 0) in vec2 frag_UV;
layout(location = 1) in vec4 frag_Col;

layout(location = 0) out vec4 colour;

layout(set = 0, binding = 0) uniform sampler2D texture_array[32];

layout(push_constant) uniform PushConsts {
    int image_offset;
    bool use_texture;
} pushConsts;

layout (constant_id = 0) const float FONT_SMOOTHING = 1.0/16.0;

void main() {
    float distance = texture(texture_array[pushConsts.image_offset],frag_UV).a;
    float alpha = smoothstep(0.5 - FONT_SMOOTHING, 0.5 + FONT_SMOOTHING, distance);
    colour = vec4(frag_Col.rgb,frag_Col.a * alpha);
}

/**[VK::End]**/